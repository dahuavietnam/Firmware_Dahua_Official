# THÔNG TIN FIRMWARE DAHUA HCVR5x04-S2_Eng #
### Phiên bản cập nhật các Firmware Dahua HCVR5x04-S2_Eng ###
+ Cập nhật ngày 19/04/2017 | Tên File: 2017-04-19_DH_HCVR5x04-S2_VinEng_P_V3.200.0004.11.R.bin  ### Hỗ trợ tiếng việt + Tiếng anh
+ Cập nhật ngày 17/04/2017 | Tên File: DH_HCVR5x04-S2_SpnEng_NP_V3.200.0004.11.R.20170417.bin  ### Hỗ trợ tiếng anh



### Sử dụng cho các mã đầu ghi ###
HCVR5104C-V2, HCVR5104HC-V2, HCVR5104H-V2, HCVR5104HE-V2
HCVR5204A-V2, HCVR5204L-V2, HCVR5208A-V2, HCVR5208L-V2, HCVR5404L-V2, HCVR5408L-V2, HCVR5804S-V2, HCVR5808S-V2
HCVR4104C-S2, HCVR4108C-S2, HCVR4104HS-S2, HCVR4108HS-S2, HCVR4104HE-S2, HCVR4108HE-S2, HCVR4204A-S2, HCVR4208A-S2, HCVR4404L-S2, HCVR4408L-S2, HCVR4804S-S2, HCVR4808S-S2, HCVR5104C-S2, HCVR5104H-S2, HCVR5104HE-S2

--------------------------------------------------------------
Cần phải cẩn thận khi thao tác Update Firmware Dahua:
+ Chọn đúng Version
+ Chọn đúng mã thiết bị

### Optimizations & bug fixes ###
1. Repair 88888 account CGI landing security issues;
2. Boot wizard P2P interface string changes;
3. Dual backup function has been incorporated;
4. telnet has been removed;
5. Fix the issue of flash program written;



### KHÔNG CHỊU TRÁCH NHIỆM KHI THIẾT BỊ "LĂN" RA CHẾT ###

* Chúng tôi không chịu trách nhiệm khi đầu ghi của bạn tự "lăn" ra chết sau khi đã Up Firmware nhé

### TRUNG TÂM HỖ TRỢ SỰ CỐ THIẾT BỊ DAHUA ###

Truy cập Website: https://gafaba.com/


### Hoặc nếu bạn không mua sản phẩm, hãy ủng hộ chúng tôi bằng việc ###
+ Trích nguồn: khi Copy bài viết

### Social Channel
https://gafabacom.atlassian.net/servicedesk/customer/portal/1
https://www.facebook.com/gafabacom/
https://www.google.com/maps/place/GaFaBa.com/@15.7147651,96.764906,5z/data=!4m8!1m2!2m1!1sgafaba!3m4!1s0x31752f1ada625d63:0x1137a4cb04d1dab5!8m2!3d15.9025448!4d105.8065263
https://www.instagram.com/gafabacom/
https://twitter.com/GaFaBacom
https://www.youtube.com/channel/UCenTi3MMQ4hcm_fsyKvpLHQ
https://www.linkedin.com/company/gafaba
https://www.pinterest.com/GaFaBacom/
https://soundcloud.com/gafaba
https://metagafaba.tumblr.com/
https://gafabacom.tumblr.com/
https://myspace.com/gafaba
https://www.flickr.com/people/gafaba/
https://github.com/GaFaBa
https://www.reddit.com/user/Gafaba
https://stackoverflow.com/users/12310885/gafaba-com
https://trello.com/b/1T2jodCk
https://trello.com/b/mLXaOQJI/wbuypro-offical
https://about.me/gafaba
https://medium.com/gafaba-com
https://www.behance.net/gafaba
https://dribbble.com/GaFaBa
https://gitlab.com/gafaba


Xin cám ơn